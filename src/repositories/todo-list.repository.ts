// Copyright IBM Corp. 2018,2019. All Rights Reserved.
// Node module: @loopback/example-todo-list
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {Getter, inject} from '@loopback/core';
import {
  DefaultCrudRepository,
  Filter,
  HasManyRepositoryFactory,
  HasOneRepositoryFactory,
  juggler,
  Options,
  repository,
} from '@loopback/repository';
import {
  Todo,
  TodoList,
  TodoListImage,
  TodoListRelations,
  TodoListWithRelations,
} from '../models';
import {TodoListImageRepository} from './todo-list-image.repository';
import {TodoRepository} from './todo.repository';

export class TodoListRepository extends DefaultCrudRepository<
  TodoList,
  typeof TodoList.prototype.id,
  TodoListRelations
> {
  public readonly todos: HasManyRepositoryFactory<
    Todo,
    typeof TodoList.prototype.id
  >;
  public readonly image: HasOneRepositoryFactory<
    TodoListImage,
    typeof TodoList.prototype.id
  >;

  constructor(
    @inject('datasources.db') dataSource: juggler.DataSource,
    @repository.getter('TodoRepository')
    protected todoRepositoryGetter: Getter<TodoRepository>,
    @repository.getter('TodoListImageRepository')
    protected todoListImageRepositoryGetter: Getter<TodoListImageRepository>,
  ) {
    super(TodoList, dataSource);
    this.todos = this.createHasManyRepositoryFactoryFor(
      'todos',
      todoRepositoryGetter,
    );
    this.image = this.createHasOneRepositoryFactoryFor(
      'image',
      todoListImageRepositoryGetter,
    );
  }

  public findByTitle(title: string) {
    return this.findOne({where: {title}});
  }

  async find(
    filter?: Filter<TodoList>,
    options?: Options,
  ): Promise<TodoListWithRelations[]> {
    const include: any = filter && filter.include;
    filter = {...filter, include: undefined};
    const res = await super.find(filter, options);

    if (include  && include.relation === 'todos') 
      await Promise.all(res.map(async r => r.todos = await this.todos(r.id).find()));

    return res;
  }

  async findById(
    id: typeof TodoList.prototype.id,
    filter?: Filter<TodoList>,
    options?: Options,
  ): Promise<TodoListWithRelations> {

    const include: any = filter && filter.include;
    filter = {...filter, include: undefined};

    const res = await super.findById(id, filter, options);

    if (include  && include.relation === 'todos') 
      res.todos = await this.todos(res.id).find();

    return res;
  }
}
