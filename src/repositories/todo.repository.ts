// Copyright IBM Corp. 2018,2019. All Rights Reserved.
// Node module: @loopback/example-todo-list
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {Getter, inject} from '@loopback/core';
import {
  BelongsToAccessor,
  DefaultCrudRepository,
  Filter,
  juggler,
  Options,
  repository,
} from '@loopback/repository';
import {Todo, TodoList, TodoRelations, TodoWithRelations} from '../models';
import {TodoListRepository} from './todo-list.repository';

export class TodoRepository extends DefaultCrudRepository<
  Todo,
  typeof Todo.prototype.id,
  TodoRelations
> {
  public readonly todoList: BelongsToAccessor<
    TodoList,
    typeof Todo.prototype.id
  >;

  constructor(
    @inject('datasources.db') dataSource: juggler.DataSource,
    @repository.getter('TodoListRepository')
    protected todoListRepositoryGetter: Getter<TodoListRepository>,
  ) {
    super(Todo, dataSource);

    this.todoList = this.createBelongsToAccessorFor(
      'todoList',
      todoListRepositoryGetter,
    );
  }

  async find(
    filter?: Filter<Todo>,
    options?: Options,
  ): Promise<TodoWithRelations[]> {
    const include: any = filter && filter.include;
    filter = {...filter, include: undefined};

    const res = await super.find(filter, options);

    if (include && include.relation === 'todoList') 
      await Promise.all(
        res.map(async r => r.todoList = await this.todoList(r.id)));
    
    return res;
  }

  async findById(
    id: typeof Todo.prototype.id,
    filter?: Filter<Todo>,
    options?: Options,
  ): Promise<TodoWithRelations> {

    const include: any = filter && filter.include;
    filter = {...filter, include: undefined};

    const res = await super.findById(id, filter, options);

    if (include && include.relation === 'todoList') 
      res.todoList = await this.todoList(res.id);
    
    return res;
  }
}
