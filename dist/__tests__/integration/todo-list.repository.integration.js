"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testlab_1 = require("@loopback/testlab");
const repositories_1 = require("../../repositories");
const helpers_1 = require("../helpers");
describe('TodoListRepository', () => {
    let todoListImageRepo;
    let todoListRepo;
    let todoRepo;
    before(async () => {
        todoListRepo = new repositories_1.TodoListRepository(helpers_1.testdb, async () => todoRepo, async () => todoListImageRepo);
        todoRepo = new repositories_1.TodoRepository(helpers_1.testdb, async () => todoListRepo);
    });
    beforeEach(helpers_1.givenEmptyDatabase);
    it('includes Todos in find method result', async () => {
        const list = await helpers_1.givenTodoListInstance(todoListRepo);
        const todo = await helpers_1.givenTodoInstance(todoRepo, { todoListId: list.id });
        const response = await todoListRepo.find({
            include: [{ relation: 'todos' }],
        });
        testlab_1.expect(testlab_1.toJSON(response)).to.deepEqual([
            Object.assign({}, testlab_1.toJSON(list), { todos: [testlab_1.toJSON(todo)] }),
        ]);
    });
    it('includes Todos in findById result', async () => {
        const list = await helpers_1.givenTodoListInstance(todoListRepo);
        const todo = await helpers_1.givenTodoInstance(todoRepo, { todoListId: list.id });
        const response = await todoListRepo.findById(list.id, {
            include: [{ relation: 'todos' }],
        });
        testlab_1.expect(testlab_1.toJSON(response)).to.deepEqual(Object.assign({}, testlab_1.toJSON(list), { todos: [testlab_1.toJSON(todo)] }));
    });
});
//# sourceMappingURL=todo-list.repository.integration.js.map