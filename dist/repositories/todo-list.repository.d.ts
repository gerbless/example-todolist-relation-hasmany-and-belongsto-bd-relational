import { Getter } from '@loopback/core';
import { DefaultCrudRepository, Filter, HasManyRepositoryFactory, HasOneRepositoryFactory, juggler, Options } from '@loopback/repository';
import { Todo, TodoList, TodoListImage, TodoListRelations, TodoListWithRelations } from '../models';
import { TodoListImageRepository } from './todo-list-image.repository';
import { TodoRepository } from './todo.repository';
export declare class TodoListRepository extends DefaultCrudRepository<TodoList, typeof TodoList.prototype.id, TodoListRelations> {
    protected todoRepositoryGetter: Getter<TodoRepository>;
    protected todoListImageRepositoryGetter: Getter<TodoListImageRepository>;
    readonly todos: HasManyRepositoryFactory<Todo, typeof TodoList.prototype.id>;
    readonly image: HasOneRepositoryFactory<TodoListImage, typeof TodoList.prototype.id>;
    constructor(dataSource: juggler.DataSource, todoRepositoryGetter: Getter<TodoRepository>, todoListImageRepositoryGetter: Getter<TodoListImageRepository>);
    findByTitle(title: string): Promise<TodoList | null>;
    find(filter?: Filter<TodoList>, options?: Options): Promise<TodoListWithRelations[]>;
    findById(id: typeof TodoList.prototype.id, filter?: Filter<TodoList>, options?: Options): Promise<TodoListWithRelations>;
}
