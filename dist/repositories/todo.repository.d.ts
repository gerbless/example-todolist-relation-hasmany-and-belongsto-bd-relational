import { Getter } from '@loopback/core';
import { BelongsToAccessor, DefaultCrudRepository, Filter, juggler, Options } from '@loopback/repository';
import { Todo, TodoList, TodoRelations, TodoWithRelations } from '../models';
import { TodoListRepository } from './todo-list.repository';
export declare class TodoRepository extends DefaultCrudRepository<Todo, typeof Todo.prototype.id, TodoRelations> {
    protected todoListRepositoryGetter: Getter<TodoListRepository>;
    readonly todoList: BelongsToAccessor<TodoList, typeof Todo.prototype.id>;
    constructor(dataSource: juggler.DataSource, todoListRepositoryGetter: Getter<TodoListRepository>);
    find(filter?: Filter<Todo>, options?: Options): Promise<TodoWithRelations[]>;
    findById(id: typeof Todo.prototype.id, filter?: Filter<Todo>, options?: Options): Promise<TodoWithRelations>;
}
