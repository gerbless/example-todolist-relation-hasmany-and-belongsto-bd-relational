import { Getter } from '@loopback/core';
import { BelongsToAccessor, DefaultCrudRepository, Filter, Options } from '@loopback/repository';
import { DbDataSource } from '../datasources';
import { TodoList, TodoListImage, TodoListImageRelations, TodoListImageWithRelations } from '../models';
import { TodoListRepository } from './todo-list.repository';
export declare class TodoListImageRepository extends DefaultCrudRepository<TodoListImage, typeof TodoListImage.prototype.id, TodoListImageRelations> {
    protected todoListRepositoryGetter: Getter<TodoListRepository>;
    readonly todoList: BelongsToAccessor<TodoList, typeof TodoListImage.prototype.id>;
    constructor(dataSource: DbDataSource, todoListRepositoryGetter: Getter<TodoListRepository>);
    find(filter?: Filter<TodoListImage>, options?: Options): Promise<TodoListImageWithRelations[]>;
    findById(id: typeof TodoListImage.prototype.id, filter?: Filter<TodoListImage>, options?: Options): Promise<TodoListImageWithRelations>;
}
